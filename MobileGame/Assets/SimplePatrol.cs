using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePatrol : MonoBehaviour
{
    public Transform[] points;
    int currentPosition;
    public float speed;

    [SerializeField]
    private Animator animator;
    void Start()
    {
        currentPosition = 0;
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (transform.position != points[currentPosition].position)
        {
            transform.position = Vector3.MoveTowards(transform.position, points[currentPosition].position, speed * Time.deltaTime);

            // Calculate the direction to the next point
            Vector3 direction = (points[currentPosition].position - transform.position).normalized;

            // Set the rotation to look in the movement direction
            if (direction != Vector3.zero)
            {
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
            }

            // Set the "isWalking" parameter in the Animator to true
            animator.SetBool("Run Forward", true);

        }
        else
        {
            currentPosition = (currentPosition + 1) % points.Length;
            animator.SetBool("Run Forward", false); //Animator to false
        }
    }
}
