using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Accessibility;

public class LevelMenu : MonoBehaviour
{
    public Button[] buttons;
    private SaveData saveData;

    //From here
    private void Awake()
    {
        int unlockLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }

        for (int i = 0; i < unlockLevel; i++)
        {
            buttons[i].interactable = true;
        }
    }

    public void OpenLevel(int levelID)
    {
        string levelName = "Level " + levelID;
        SceneManager.LoadScene(levelName);
    }

    //To here
    //Rehope Games. (May 2023). Create LEVEL MENU in Unity: UI Design & Level Locking/Unlocking System!. [Online]. Youtube.Available at: https://www.youtube.com/watch?v=2XQsKNHk1vk [Accessed 3 October 2023].
}
