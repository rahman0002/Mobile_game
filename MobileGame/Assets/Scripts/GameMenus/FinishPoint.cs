using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using System;

public class FinishPoint : MonoBehaviour
{
    public GameObject player;
    public InterstitialAds interstitialAds;
    public float startTime;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        StartTimer();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player") && player.GetComponent<PlayerInventory>().NumberOfItems == 4)
        {
            StopTimer();
            float completeTime = Time.time - startTime;
            if (completeTime < 30f)
            {
                if (interstitialAds != null)
                {
                    interstitialAds.LoadAd();
                    interstitialAds.ShowAd();
                }

                if (interstitialAds == null)
                {
                    Debug.Log("ads is null");
                }
            }
            UnlockNewLevel();
            //transition to next level
            SceneController.instance.nextLevel();
        }
    }

    //From here
    void UnlockNewLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        {
            PlayerPrefs.SetInt("ReachedIndex", SceneManager.GetActiveScene().buildIndex + 1);
            PlayerPrefs.SetInt("UnlockedLevel", PlayerPrefs.GetInt("UnlockedLevel", 1) + 1);

            //riset data here
            PlayerPrefs.Save();
            Debug.Log(PlayerPrefs.GetInt("ReachedIndex"));
        }
    }
    //To here
    //Rehope Games. (May 2023). Create LEVEL MENU in Unity: UI Design & Level Locking/Unlocking System!. [Online]. Youtube.Available at: https://www.youtube.com/watch?v=2XQsKNHk1vk [Accessed 3 October 2023].
    private void StartTimer()
    {
        startTime = Time.time;
    }

    private void StopTimer()
    {
        // You may want to record the completion time for further use or logging
        float completionTime = Time.time - startTime;

        // Optionally, you can log the completion time
        Debug.Log("Level completion time: " + completionTime + " seconds");
        startTime = 0f;
    }
}
