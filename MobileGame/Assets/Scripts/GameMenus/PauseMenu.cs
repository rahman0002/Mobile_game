using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pauseMenuUI;
    public void Menu()
    {

        Time.timeScale = 1f;
        SceneManager.LoadScene("Main Menu");
    }
     public void Resume()
    {

        Time.timeScale = 1f;
        pauseMenuUI.SetActive(false);
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Pause()
    {
        Debug.Log("Button getting pressed");
        Time.timeScale =0f;
        pauseMenuUI.SetActive(true); 
    }

}
