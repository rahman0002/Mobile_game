using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : MonoBehaviour
{
    public void QuitGame()
    {
        Vibrate(1);
        Application.Quit();
    }

    public void Vibrate(long milliseconds)
    {
        // Check if running on Android
        if (Application.platform == RuntimePlatform.Android)
        {
            // Create a Vibrator object
            AndroidJavaObject vibrator = new AndroidJavaClass("android.os.Vibrator").CallStatic<AndroidJavaObject>("getSystem");

            // Vibrate for the specified duration
            vibrator.Call("vibrate", milliseconds);
            Debug.Log("is vibrating");
        }
    }
}

