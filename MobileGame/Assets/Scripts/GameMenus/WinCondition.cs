using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinCondition : MonoBehaviour
{
    public GameObject player;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player") && player.GetComponent<PlayerInventory>().NumberOfItems == 4)
        {
            UnlockNewLevel();
            //transition to next level
            SceneController.instance.nextLevel();
        }
    }

//From here
    void UnlockNewLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        {
            PlayerPrefs.SetInt("ReachedIndex", SceneManager.GetActiveScene().buildIndex + 1);
            PlayerPrefs.SetInt("UnlockedLevel", PlayerPrefs.GetInt("UnlockedLevel", 1) + 1);

            //riset data here
            PlayerPrefs.Save();
            Debug.Log(PlayerPrefs.GetInt("ReachedIndex"));
        }
    }
}
//To here
//Rehope Games. (May 2023). Create LEVEL MENU in Unity: UI Design & Level Locking/Unlocking System!. [Online]. Youtube.Available at: https://www.youtube.com/watch?v=2XQsKNHk1vk [Accessed 3 October 2023].
