using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseCondition : MonoBehaviour
{
    [SerializeField]
    private GameObject gameOverscreen; // Reference to the Game Over canvas
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // Display the game over canvas if it's assigned in the Inspector
            if (gameOverscreen != null)
            {
                gameOverscreen.SetActive(true);
            }
            //Pause the Game
            Time.timeScale = 0;
        }
    }
}
