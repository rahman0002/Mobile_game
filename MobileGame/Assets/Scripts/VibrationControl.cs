using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CandyCoded.HapticFeedback;
using UnityEngine.U2D;


public class VibrationControl : MonoBehaviour
{
    public const string key = "VibrationEnagled";
    private bool isToggleEnabled = true;

    public void Start()
    {
        // Load the saved toggle state
        if (PlayerPrefs.HasKey(key))
        {
            isToggleEnabled = PlayerPrefs.GetInt(key) == 1;
        }
        userToggle(isToggleEnabled);
    }
    public void Vibration()
     {
        Debug.Log("Vibration is working");
        HapticFeedback.MediumFeedback();
     }

    public void userToggle(bool isToggleEnabled)
    {
        isToggleEnabled = !isToggleEnabled;

        if (isToggleEnabled)
        {
            Vibration();
        }

        //Save currrent toggle state
        PlayerPrefs.SetInt(key, isToggleEnabled ? 1 : 0);
        PlayerPrefs.Save();

        // Debug log to check if PlayerPrefs is correctly saved
        Debug.Log("Saved Vibration State: " + isToggleEnabled);
    }
}