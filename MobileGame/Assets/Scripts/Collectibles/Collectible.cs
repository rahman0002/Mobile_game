using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float rotationSpeed = 30f;
    private void OnTriggerEnter(Collider other)
    {
        PlayerInventory playerInventory = other.GetComponent<PlayerInventory>();
        if ( playerInventory != null) 
        {
            Handheld.Vibrate();
            playerInventory.ItemCollected();
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        //Rotate the object around its Y-axis(upward) at the specified speed.
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
}
