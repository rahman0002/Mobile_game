using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControl : MonoBehaviour
{
    private Vector3 gyroRotation;

    public float rotationSpeed = 15.0f;
    public float maxRotationSpeed = 20.0f;
    void Start()
    {

        if (SystemInfo.supportsGyroscope)
        {
            //Check if gyro is enabled on the device
            Input.gyro.enabled = true;
            Debug.Log("Gyro Enable");
        }
    }

    void FixedUpdate()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyroRotation = Input.gyro.rotationRateUnbiased; 

            // Apply rotation to the plane only on the X and Z axes with sensitivity and clamping
            float rotationX = -gyroRotation.x * rotationSpeed * Time.fixedDeltaTime;
            float rotationY = -gyroRotation.y * rotationSpeed * Time.fixedDeltaTime;

            // Limit the rotation speed
            rotationX = Mathf.Clamp(rotationX, -maxRotationSpeed, maxRotationSpeed);
            rotationY = Mathf.Clamp(rotationY, -maxRotationSpeed, maxRotationSpeed);


            // Apply rotation to the plane only on the X and Z axes
            transform.Rotate(rotationX, 0, rotationY);
        }
    }
}
