using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController instance;

//From here
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            
        }

        else
        {
            Destroy(gameObject);
        }
    }
    public void nextLevel()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void loadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }
}
//To here
//Rehope Games. (May 2023). Create LEVEL MENU in Unity: UI Design & Level Locking/Unlocking System!. [Online]. Youtube.Available at: https://www.youtube.com/watch?v=2XQsKNHk1vk [Accessed 3 October 2023].