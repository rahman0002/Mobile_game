using UnityEngine;
using UnityEngine.Advertisements;

//From here
public class InterstitialAds : MonoBehaviour, IUnityAdsLoadListener, IUnityAdsShowListener
{
    [SerializeField] string _androidAdUnitId = "Interstitial_Android";
    [SerializeField] string _iOSAdUnitId = "Interstitial_iOS";
    string _adUnitId;

    void Awake()
    {
        // Get the Ad Unit ID for the current platform:
        _adUnitId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? _iOSAdUnitId
            : _androidAdUnitId;
    }

    // Load content to the Ad Unit:
    public void LoadAd()
    {
        // IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
        Debug.Log("Loading Ad: " + _adUnitId);
        Advertisement.Load(_adUnitId, this);
    }

    public void AdReady()
    {
        LoadAd();
        ShowAdWithProbability();
    }

    // Show the loaded content in the Ad Unit with a probability of 33%:
    public void ShowAdWithProbability()
    {
        // Generate a random value between 0 and 1
        float randomValue = Random.value;

        // Set the probability of showing the ad (e.g., 33% or 0.33)
        float showProbability = 0.40f;

        // Check if the random value is less than the show probability
        if (randomValue < showProbability)
        {
            // Show the ad if the condition is met
            ShowAd();
        }
        else
        {
            // Log that the ad won't be shown in this case
            Debug.Log("Ad won't be shown this time.");
        }
    }

    // Show the loaded content in the Ad Unit:
    public void ShowAd()
    {
        // Note that if the ad content wasn't previously loaded, this method will fail
        Debug.Log("Showing Ad: " + _adUnitId);
        Advertisement.Show(_adUnitId, this);
    }

    // Implement Load Listener and Show Listener interface methods: 
    public void OnUnityAdsAdLoaded(string adUnitId)
    {
        // Optionally execute code if the Ad Unit successfully loads content.
    }

    public void OnUnityAdsFailedToLoad(string _adUnitId, UnityAdsLoadError error, string message)
    {
        Debug.Log($"Error loading Ad Unit: {_adUnitId} - {error.ToString()} - {message}");
        // Optionally execute code if the Ad Unit fails to load, such as attempting to try again.
    }

    public void OnUnityAdsShowFailure(string _adUnitId, UnityAdsShowError error, string message)
    {
        Debug.Log($"Error showing Ad Unit {_adUnitId}: {error.ToString()} - {message}");
        // Optionally execute code if the Ad Unit fails to show, such as loading another ad.
    }

    public void OnUnityAdsShowStart(string _adUnitId) { }
    public void OnUnityAdsShowClick(string _adUnitId) { }
    public void OnUnityAdsShowComplete(string _adUnitId, UnityAdsShowCompletionState showCompletionState) { }
}
//To here
//Unity. (n.d.). Implementing interstitial ads in Unity. [Online]. Unity. Available at: https://docs.unity.com/ads/en-us/manual/ImplementingBasicAdsUnity [Accessed 23 November 2023].
