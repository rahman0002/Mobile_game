using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class PlayerInventory : MonoBehaviour
{
    // Start is called before the first frame update
    public int NumberOfItems;
    public UnityEvent<PlayerInventory> OnItemCollected;

    // Update is called once per frame
    public void ItemCollected()
    {
        NumberOfItems++;
        OnItemCollected?.Invoke(this);
    }

}
