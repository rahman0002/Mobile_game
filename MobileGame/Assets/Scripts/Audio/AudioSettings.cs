using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class AudioSettings : MonoBehaviour
{
    [SerializeField]
    private AudioMixer audioMixer;

    [SerializeField]
    private Slider musicSlider;

    public void Start()
    {
        if (PlayerPrefs.HasKey("musicVolume"))
        {
            LoadVolume();
        }

        else
        {
            SetMusicVolume();
        }
    }
    //From here
    public void SetMusicVolume()
    {
        float volume = musicSlider.value;
        audioMixer.SetFloat("music", volume);
        PlayerPrefs.SetFloat("musicVolume", volume);
    }
    //To here
    //Rehope Games. (March 2023). Unity AUDIO Volume Settings Menu Tutorial. [Online]. Youtube. Available at: https://www.youtube.com/watch?v=G-JUp8AMEx0 [Accessed 12 December 2023].

    private void LoadVolume()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        SetMusicVolume();
    }
}
